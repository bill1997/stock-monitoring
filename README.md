# 🔥🔥股票监控
![stock-monitoring](https://cdn.jsdelivr.net/gh/dromara/hertzbeat@gh-pages/img/badge/threshold.svg)
![stock-monitoring](https://cdn.jsdelivr.net/gh/dromara/hertzbeat@gh-pages/img/badge/database-monitor.svg)
![stock-monitoring](https://cdn.jsdelivr.net/gh/dromara/hertzbeat@gh-pages/img/badge/os-monitor.svg)
![stock-monitoring](https://cdn.jsdelivr.net/gh/dromara/hertzbeat@gh-pages/img/badge/custom-monitor.svg)
## 🎡 介绍
-  **stock-monitoring**  是一个拥有强大自定义监控能力，无需Agent的实时监控系统。应用服务，中间件，云原生等监控，阈值告警，告警通知(微信/钉钉/飞书)。
- 我们也提供了对应的python版本监控云，中小团队和个人无需再为了监控自己的网站资源，而去部署一套繁琐的监控系统，登录即可免费开始。
-  **stock-monitoring**   支持自定义监控 ,只用通过配置config文件我们就可以自定义需要的监控类型和指标，来满足常见的个性化需求。
-  **stock-monitoring**   模块化，各个模块解耦合，方便理解与定制开发。
-  **stock-monitoring**   支持更自由化的告警配置(计算表达式)，支持告警通知, 钉钉微信飞书, webhook等及时通知送达。
- 我们将Http,Jmx,Ssh,Jdbc等协议规范可配置化，您仅仅只需配置config就能使用这些协议去自定义采集任何您想要采集的指标。
-  **stock-monitoring**   的强大自定义，多类型支持，易扩展，低耦合，希望能帮助开发者和中小团队快速搭建自有监控系统。

## 🥐 软件架构
```
根目录
config （配置文件）
 code.json(参数配置) 
main.py (stock-monitoring Demo源码，可以直接运行查看效果)
```

## 🐕 快速开始
> 如果您是想将stock-monitoring部署到内网环境搭建监控系统，请参考下面的部署文档进行操作。
##### 本地代码启动
1. git clone https://gitee.com/phlio/stock-monitoring.git
2. pip3 install -r requirement.txt
3. **[必须]** 配置 stock-monitoring 的配置文件 `stock-monitoring/config/code.json`

```
{
  "codes":[
      { "code": "股票代码", "up": 涨幅告警阈值,"down": 跌幅告警阈值 }, #通用股票代码 sz/sh/bj + 股票代码
      { "code": "股票代码", "up": 涨幅告警阈值,"down": 跌幅告警阈值 }, #例如sh588000/sz000001/bj831010
      { "code": "股票代码", "up": 涨幅告警阈值,"down": 跌幅告警阈值 } #涨跌幅告警阈值，例如2.5/-1.5
  ],
  "lark": [
    {"webhook": "https://open.feishu.cn/open-apis/bot/hook/xxxxxx"} #飞书对应的群机器人webhook地址
  ],
  "weChat": [
    {"webhook": "https://sc.ftqq.com/xxxxxx.send"} #微信server酱对应的api地址
  ],
  "dingDing": [
    {"webhook": "https://oapi.dingtalk.com/robot/send?access_token=xxxxx"} #钉钉对应的群机器人webhook地址
  ],
  "pushMode": [
    {"mode": "lark"} #推送方式 -> lark/weChat/dingDing
  ]
}
```
4. 运行主程序python3 main.py 
5. 实现定时执行
`while [ 1 ];do python3 main.py;sleep 60s;done`


## 🍉 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 🛡️ License
[`Apache License, Version 2.0`](https://www.apache.org/licenses/LICENSE-2.0.html)